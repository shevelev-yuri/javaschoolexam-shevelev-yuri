package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws CannotBuildPyramidException if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        for (Integer element : inputNumbers) {
            if (element == null) {
                throw new CannotBuildPyramidException();
            }
        }

        int height = getHeight(inputNumbers.size());
        int width = 2 * height - 1;
        int[][] pyramid = new int[height][width];

        Collections.sort(inputNumbers);

        int offset = width / 2;
        int inputIndex = 0;
        for (int row = 0; row < height; row++) {
            int addedNumbers = 0;
            int column = 0;
            while (addedNumbers < (row + 1)) {
                int number = inputNumbers.get(inputIndex);
                pyramid[row][column + offset] = number;
                addedNumbers++;
                inputIndex++;
                column += 2;
            }
            offset--;
        }
        return pyramid;
    }

    /**
     * Calculates the height of the pyramid based on the number of elements and determines whether
     * the pyramid can be built.
     *
     * @param listSize size of the input list
     * @return height of the pyramid
     */
    private int getHeight(int listSize) {
        int height = 0;
        while (listSize > 0) {
            listSize -= height;
            height++;
        }
        if (listSize < 0) {
            throw new CannotBuildPyramidException();
        } else {
            return height - 1;
        }
    }
}
