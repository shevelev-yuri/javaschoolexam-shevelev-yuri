package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Represents number of digits after the comma.
     */
    private static final int ROUND_PLACES = 4;

    /**
     * Evaluate statement represented as String.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return String value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) return null;
        try {
            String output = getExpression(statement);
            double result = counting(output);
            if (Double.isFinite(result) && Double.compare(result, StrictMath.rint(result)) == 0) {
                return Integer.toString((int) result);
            }
            return Double.toString(roundResult(result));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts the statement to prefix notation.
     *
     * @param statement to be converted
     * @return converted statement for counting method
     */
    private String getExpression(String statement) {
        StringBuilder output = new StringBuilder();
        Deque<Character> operDeque = new ArrayDeque<>();

        for (int i = 0; i < statement.length(); i++) {
            if (Character.isDigit(statement.charAt(i))) {
                while (!isDelimiter(statement.charAt(i)) && !isOperator(statement.charAt(i))) {
                    output.append(statement.charAt(i));
                    i++;
                    if (i == statement.length()) break;
                }
                output.append(" ");
                i--;
            }

            if (isOperator(statement.charAt(i))) {
                if (statement.charAt(i) == '(') {
                    operDeque.push(statement.charAt(i));
                } else if (statement.charAt(i) == ')') {
                    char operator = operDeque.pop();

                    while (operator != '(') {
                        output.append(operator).append(" ");
                        operator = operDeque.pop();
                    }
                } else {
                    if (!operDeque.isEmpty())
                        if (getPriority(statement.charAt(i)) <= getPriority(operDeque.peek()))
                            output.append(operDeque.pop().toString()).append(" ");
                    operDeque.push(statement.charAt(i));
                }
            }
        }
        while (!operDeque.isEmpty())
            output.append(operDeque.pop()).append(" ");
        return output.toString();
    }

    /**
     * Checks whether the character is delimiter.
     *
     * @param c character to be checked
     * @return {@code true} or {@code false}
     */
    private boolean isDelimiter(char c) {
        return (" ".indexOf(c) != -1);
    }

    /**
     * Checks whether the character is operator.
     *
     * @param c character to be checked
     * @return {@code true} or {@code false}
     */
    private boolean isOperator(char c) {
        return ("+-/*()".indexOf(c) != -1);
    }

    /**
     * Checks operator priority for evaluation.
     *
     * @param operator operator to be checked
     * @return the priority needed to perform
     */
    private byte getPriority(char operator) {
        switch (operator) {
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
                return 2;
            case '-':
                return 3;
            case '*':
            case '/':
                return 4;
            default:
                return 5;
        }
    }

    /**
     * Evaluates statement converted to prefix notation.
     *
     * @param input converted statement
     * @return result of statement evaluation
     */
    private double counting(String input) {
        double result = 0;
        Deque<Double> tmp = new ArrayDeque<>();

        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                StringBuilder sb = new StringBuilder();

                while (!isDelimiter(input.charAt(i)) && !isOperator(input.charAt(i))) {
                    sb.append(input.charAt(i));
                    i++;
                    if (i == input.length()) break;
                }
                tmp.push(Double.parseDouble(sb.toString()));
                i--;
            } else if (isOperator(input.charAt(i))) {
                double first = tmp.pop();
                double second = tmp.pop();

                switch (input.charAt(i)) {
                    case '+':
                        result = second + first;
                        break;
                    case '-':
                        result = second - first;
                        break;
                    case '*':
                        result = second * first;
                        break;
                    case '/':
                        result = second / first;
                        break;
                }
                tmp.push(result);
            }
        }
        return tmp.peek();
    }

    /**
     * Rounds the result of evaluation.
     *
     * @param value result to be rounded
     * @return rounded result
     */
    private double roundResult(double value) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(ROUND_PLACES, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}