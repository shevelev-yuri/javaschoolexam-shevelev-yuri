package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     * @throws IllegalArgumentException if x or y is null
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        boolean isEqual = true;
        int cursor = 0;
        for (int indexOfX = 0; indexOfX < x.size(); indexOfX++) {
            if (!isEqual) break;
            isEqual = false;

            for (int indexOfY = cursor; indexOfY < y.size(); indexOfY++) {
                if (x.get(indexOfX).equals(y.get(indexOfY))) {
                    isEqual = true;
                    cursor = indexOfY + 1;
                    break;
                }
            }
        }
        return isEqual;
    }
}